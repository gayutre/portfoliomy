/* --- toggle menu navigator--- */

let menuIcon = document.querySelector("#menu-icons");
let navbar = document.querySelector(".navbar");

menuIcon.onclick = () => {
    menuIcon.classList.toggle("bx-x");
    navbar.classList.toggle("active");

}


/* ---scroll section active link---*/

const sections = document.querySelectorAll('section');

const navLinks = document.querySelectorAll(' nav a');

const isInViewport = (element) => {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight)
    );
};


const toggleActiveLink = () => {
    sections.forEach((section, index) => {
        const link = navLinks[index];
        if (isInViewport(section)) {
            link.classList.add('active');
        } else {
            link.classList.remove('active');
        }
    });
    let header = document.querySelector('header');

    header.classList.toggle('sticky', window.scrollY > 100);
};

window.addEventListener('scroll', toggleActiveLink);



/*----remove the toggle icon & navbar when click navbar link(scroll)------*/

menuIcon.classList.remove("bx-x");
navbar.classList.remove("active");

/*---scroll reveal---*/

// Select the element to observe
const container = document.querySelector('.home-content');
const about = document.querySelector('.about-section');
const project = document.querySelector('.project-section');
const contact = document.querySelector('.contact-section');

// Create an intersection observer instance
const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            entry.target.classList.add('active'); // Add active class when in viewport
        }
    });
});

// Start observing the container
observer.observe(container);
observer.observe(about);
observer.observe(project);
observer.observe(contact);

